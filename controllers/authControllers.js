const jwt = require('jsonwebtoken');
const express = require('express');
const accessTokenSecret = 'hfrjjfrjefrkkefrkldeko3jfjnrrf';
const User = require("../models/userModelDB");

exports.registration = function(req, res){
    User.create({userName:req.body.Name, userSurname:req.body.Surname, userEmail:req.body.Email, userPassword:req.body.Password, userRole:req.body.Role}, function(err, doc){     
        if (err) {return console.log(err);}
        res.send(`Новый пользователь зарегистрирован ${doc}`); 
    });         
};

exports.login = async function(req, res){
    const user = await User.findOne({userEmail:req.body.Email, userPassword:req.body.Password});
    if (user){
        const accessToken = jwt.sign({userEmail: user.userEmail, userRole: user.userRole, _id:user._id, userName:user.userName}, accessTokenSecret);
        res.json({accessToken});
    } else{
        res.send(`Неверные данные пользователя`);
    }
};
