const Category = require("../models/categoryModelDB");
const jwt = require('jsonwebtoken');
const { request, response } = require("express");
const { JsonWebTokenError } = require("jsonwebtoken");
const accessTokenSecret = 'hfrjjfrjefrkkefrkldeko3jfjnrrf';

exports.authJWT = function (request, response, next){
    const authHeader = request.headers.authorization;
    if(authHeader){
        const token = authHeader.split(' ')[1];
        jwt.verify(token, accessTokenSecret, (err, user)=>{
            if (err){
                return response.send("Неверный токен");
            }
            request.user = user;
            next();
        });
    } else{
        response.send("Не проведена авторизация");
    }
};

exports.checkRole = function (request, response, next){
    if (request.user.userRole !== "admin"){
        return response.send("Доступ запрещен");    
    }
    next();
};

exports.create = function(request, response){
    Category.create({categoryName:request.body.Name}, function(err, doc){     
        if (err) {return response.send(err);}
        response.send(`Категория создана ${doc}`); 
    });      
};

exports.delete = function(request, response){
    Category.deleteOne({categoryName:request.body.Name}, function (err, result){      
        if (err) {return response.send(err);}
        response.send(`Категория удалена`); 
    });       
};

exports.update = function(request, response){
    Category.updateOne({categoryName:request.body.oldName}, {categoryName:request.body.newName}, function(err, result){
        if (err) {return response.send(err);}
        response.send(`Имя категории обновлено`);
    });        
};

exports.getByID = function(request, response){
    Category.findById({_id:request.body.Id}, function(err, doc){           
        if (err) {return response.send(err);}
        response.send(`<h3> Данные получены: ${doc} <h3>`);     
    });      
};

exports.getAll = function(request, response){
    Category.find({}, function(err, doc){           
        if (err) {return response.send(err);}
        response.send(`<h3> Данные получены: ${doc} <h3>`);     
    });      
};

