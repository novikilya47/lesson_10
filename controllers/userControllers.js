const User = require("../models/userModelDB");
const jwt = require('jsonwebtoken');
const { request, response } = require("express");
const { JsonWebTokenError } = require("jsonwebtoken");
const accessTokenSecret = 'hfrjjfrjefrkkefrkldeko3jfjnrrf';

exports.authJWT = function (request, response, next){
    const authHeader = request.headers.authorization;
    if(authHeader){
        const token = authHeader.split(' ')[1];
        jwt.verify(token, accessTokenSecret, (err, user)=>{
            if (err){
                return response.send("Неверный токен");
            }
            request.user = user;
            next();
        });
    } else{
        response.send("Не проведена авторизация");
    }
};

exports.checkRole = function (request, response, next){
    if (request.user.userRole !== "admin"){
        return response.send("Доступ запрещен");    
    }
    next();
};
 
exports.create = function(request, response){
    User.create({userName:request.body.Name, userSurname:request.body.Surname, userEmail:request.body.Email, userPassword:request.body.Password}, function(err, doc){     
        if (err) {return response.send(err);}
        response.send(`Пользователь создан ${doc}`); 
    });      
};

exports.delete = function(request, response){
    User.deleteOne({userEmail:request.body.Email}, function (err, result){      
        if (err) {return response.send(err);}
        response.send(`Пользователь удален`); 
    });       
};

exports.update = function(request, response){
    if (request.user.userRole == "admin"){
        User.updateOne({userName:request.body.oldName}, {userName:request.body.newName}, function(err, result){
            if (err) {return response.send(err);}
            response.send(`Имя пользователя обновлено`);
        });
    }  else {
        User.updateOne({userName:request.user.userName}, {userName:request.body.newName}, function(err, result){
            if (err) {return response.send(err);}
            response.send(`Имя пользователя обновлено`);
        });
    }      
};

exports.getByID = function(request, response){
    User.findById({_id:request.body.Id}, function(err, doc){           
        if (err) {return response.send(err);}
        response.send(`<h3> Данные получены: ${doc} <h3>`);     
    });      
};

exports.getAll = function(request, response){
    User.find({}, function(err, doc){           
        if (err) {return response.send(err);}
        response.send(`<h3> Данные получены: ${doc} <h3>`);     
    });      
};

