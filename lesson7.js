const express = require('express');
const app = express();
const auth = require("./routes/auth");
const user = require('./routes/user');
const category = require('./routes/category');
const mongoose = require("mongoose");


app.use("/auth", auth);
app.use("/user", user);
app.use("/category", category);

mongoose.connect("mongodb://localhost:27017/BD", { useUnifiedTopology: true, useNewUrlParser:true}, function(err){
    if(err) {return console.log('Something broke!');}
    app.listen(3000, function(){
        console.log("Сервер работает");
    });
});


