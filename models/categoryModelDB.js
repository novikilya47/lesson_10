const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const autopopulate = require("mongoose-autopopulate");
 
const CategoryScheme = new Schema(
    {categoryName: String,},
    { versionKey: false });

CategoryScheme.plugin(require("mongoose-autopopulate"));
const Category = mongoose.model("Category", CategoryScheme); 
module.exports = Category;