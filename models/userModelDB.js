const mongoose = require("mongoose");
const Schema = mongoose.Schema;
 
const UserScheme = new Schema(
    {userName: String, 
    userSurname: String, 
    userPassword: String,
    userEmail: String, 
    userRole: {type : String, default : "user"}, 
    },
    { versionKey: false });

const User = mongoose.model("User", UserScheme); 
module.exports = User;