const express = require('express');
const auth = express.Router();
const urlencodedParser = express.urlencoded({extended: true});
const authControllers = require("../controllers/authControllers");
const userController = require("../routes/user");
const categoryController = require("../routes/category");

auth.post('/registration', urlencodedParser, authControllers.registration);
auth.post('/login', urlencodedParser, authControllers.login, userController, categoryController);

module.exports = auth;