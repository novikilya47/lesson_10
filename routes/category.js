const express = require('express');
const category = express.Router();
const urlencodedParser = express.urlencoded({extended: true});
const categoryController = require("../controllers/categoryControllers");

category.post("/create", urlencodedParser, categoryController.authJWT, categoryController.checkRole, categoryController.create);
category.delete("/delete", urlencodedParser, categoryController.authJWT, categoryController.checkRole, categoryController.delete);
category.put("/update", urlencodedParser, categoryController.authJWT, categoryController.checkRole, categoryController.update);
category.get("/getByID", urlencodedParser, categoryController.authJWT, categoryController.getByID);
category.get("/getAll", urlencodedParser, categoryController.authJWT, categoryController.getAll);

module.exports = category;