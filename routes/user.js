const express = require('express');
const user = express.Router();
const urlencodedParser = express.urlencoded({extended: true});
const userController = require("../controllers/userControllers");

user.post("/create", urlencodedParser, userController.authJWT, userController.checkRole, userController.create);
user.delete("/delete", urlencodedParser, userController.authJWT, userController.checkRole, userController.delete);
user.put("/update", urlencodedParser, userController.authJWT, userController.update);
user.get("/getByID", urlencodedParser, userController.authJWT, userController.getByID);
user.get("/getAll", urlencodedParser, userController.authJWT, userController.getAll);

module.exports = user;
